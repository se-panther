package panther.ui;

import java.sql.*;

import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import panther.db.*;

public class StudentTableModel
	extends AbstractTableModel
{
	ArrayList<Student> students;

	public StudentTableModel()
	{
		students = MySQLManager.get_students();
	}

	public String getColumnName(int col)
	{
		if (col == 0) {
			return "Students";
		}

		return null;
	}

	public int getColumnCount()
	{
		return 1;
	}

	public int getRowCount()
	{
		return students.size();
	}

	public Object getValueAt(int row, int col)
	{
		if (col == 0 && row < getRowCount()) {
			return students.get(row)/*.getName()*/;
		}

		return null;
	}
}
