package panther.ui;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import panther.db.*;

public class StudentSelectionListener
	implements ListSelectionListener
{
	private JTable master;
	private JTable detail;
	private JButton b_scales;

	public StudentSelectionListener(JTable master, JTable detail, JButton scales)
	{
		this.master = master;
		this.detail = detail;
		this.b_scales = scales;
	}

	public void valueChanged(ListSelectionEvent e)
	{
		if (e.getValueIsAdjusting()) return;

		int m_idx = master.convertRowIndexToModel(master.getSelectedRow());

		b_scales.setEnabled(true);

		Object student_o = master.getModel().getValueAt(m_idx, 0);
		if (!(student_o instanceof Student))
			return;

		Student student = (Student) student_o;

		TableModel tmodel_o = detail.getModel();
		if (!(tmodel_o instanceof SessionTableModel))
			return;

		SessionTableModel tmodel = (SessionTableModel) tmodel_o;
		tmodel.update_student_sessions(student.get_id());
	}
}
