package panther.ui;

import java.awt.BorderLayout;
import javax.swing.*;

public class SessionPanel
	extends JPanel
{
	public SessionPanel()
	{
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createTitledBorder("Session")));

		SessionTable table = new SessionTable();
		add(new JScrollPane(table));
	}
}
