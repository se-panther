package panther.ui;

import java.awt.event.*;
import javax.swing.*;

import org.jfree.ui.*;

public class PtMenuBar
	extends JMenuBar
{
	public PtMenuBar(ActionListener listener)
	{
		JMenu jm;
		JMenuItem jmi;

		jm = new JMenu("File");
		jmi = new JMenuItem("quit");
		jmi.addActionListener(listener);
		jm.add(jmi);

		add(jm);
	}

}
