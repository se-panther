package panther.ui;

import java.util.Date;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import panther.db.*;

public class SessionTableModel
	extends AbstractTableModel
{
	private int n_rows;

	private RepetoireList l_repetoire;
//	private final static int N_DATES = 5;
//	private Date[] dates;

	public SessionTableModel()
	{
		l_repetoire = null;

		n_rows = 0;
//		dates = new Date[N_DATES];
//		for (int i=0; i<N_DATES; i++) {
//			dates[i] = new Date();
//		}
	}

	public String getColumnName(int col)
	{
		if (col == 0) {
			return "Session";
		}

		return null;
	}

	public int getColumnCount()
	{
		return 1;
	}

	public int getRowCount()
	{
		return n_rows;
	}

	public Object getValueAt(int row, int col)
	{
		if (col == 0 && n_rows > 0) {
			if (l_repetoire != null) {
				return l_repetoire.get(row);
			}
//			return dates[row];
		}

		return null;
	}

	private void update_n_rows()
	{
		n_rows = l_repetoire.size();
	}

	public void update_student_sessions(int sid)
	{
		l_repetoire = MySQLManager.get_student_repetoire(sid);

		update_n_rows();

		fireTableDataChanged();
	}
}
