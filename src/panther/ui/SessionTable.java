package panther.ui;

import javax.swing.*;
import javax.swing.table.*;

public class SessionTable
	extends JTable
{
	public SessionTable()
	{
		setModel(new SessionTableModel());
		setColumnSelectionAllowed(false);

		getSelectionModel().setSelectionMode(
				ListSelectionModel.SINGLE_SELECTION);
	}
}
