package panther.ui;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import panther.ui.listeners.*;

public class PtMainPanel
	extends JPanel
{
	public PtMainPanel()
	{
		setLayout(new BorderLayout());

		JButton b_gen_scales = new JButton("Scales");
		JButton b_gen_sightreading = new JButton("Sightreadig");

		JTable tbl_student = new StudentTable();
		JTable tbl_session = new SessionTable();

		b_gen_scales.setEnabled(false);
		b_gen_scales.setActionCommand("genreport.scales");
		b_gen_scales.addActionListener(
				new ScalesButtonActionListener(tbl_student));

		b_gen_sightreading.setEnabled(false);
		/* BoxLayout along x-axis may not be what we want here, this is just a
		 * quick and dirty solution to get panels to resize with frame resize
		 * events. */
//		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
//		setLayout(new BorderLayout());

		tbl_student.getSelectionModel().addListSelectionListener(
				new StudentSelectionListener(
					tbl_student,
					tbl_session,
					b_gen_scales)
				);
		tbl_session.getSelectionModel().addListSelectionListener(
				new SessionSelectionListener(tbl_session));

		JPanel pnl_student = new JPanel();

		pnl_student.setLayout(new BorderLayout());
		pnl_student.setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createTitledBorder("Student")));
		pnl_student.add(new JScrollPane(tbl_student));

		JPanel pnl_session = new JPanel();

		pnl_session.setLayout(new BorderLayout());
		pnl_session.setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createTitledBorder("Session")));
		pnl_session.add(new JScrollPane(tbl_session));

		JPanel pnl_lists = new JPanel();
		pnl_lists.setLayout(new BoxLayout(pnl_lists, BoxLayout.X_AXIS));
		pnl_lists.add(pnl_student);
		pnl_lists.add(pnl_session);

		JPanel pnl_buttons = new JPanel();
		pnl_buttons.add(b_gen_scales);
		pnl_buttons.add(b_gen_sightreading);

		add(pnl_buttons, BorderLayout.NORTH);
		add(pnl_lists, BorderLayout.CENTER);
	}
}
