package panther.ui;

import java.awt.BorderLayout;
import javax.swing.*;

public class StudentPanel
	extends JPanel
{
	public StudentPanel()
	{
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createTitledBorder("Student")));

		StudentTable table = new StudentTable();
		add(new JScrollPane(table));
	}
}
