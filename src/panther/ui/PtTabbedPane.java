package panther.ui;

import javax.swing.*;

public class PtTabbedPane
	extends JTabbedPane
{
	public PtTabbedPane()
	{
		setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		setName("PtTabbedPane");

		/* the 'main' tab should be the only tab available after launch.
		 * 'report' tabs are added through user interaction within the main tab
		 * */
		addTab("Main", new PtMainPanel());
	}
}
