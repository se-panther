package panther.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

public class StudentTable
	extends JTable
{
	public StudentTable()
	{
		setModel(new StudentTableModel());
		setColumnSelectionAllowed(false);

		/* limit user selection to only one student at a time */
		getSelectionModel().setSelectionMode(
				ListSelectionModel.SINGLE_SELECTION);
	}
}
