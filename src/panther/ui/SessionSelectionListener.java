package panther.ui;

import java.awt.Container;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import panther.db.*;

public class SessionSelectionListener
	implements ListSelectionListener
{
	private JTable tbl_session;

	public SessionSelectionListener(JTable tbl_session)
	{
		this.tbl_session = tbl_session;
	}

	public void valueChanged(ListSelectionEvent e)
	{
		int row = tbl_session.getSelectedRow();

		if (e.getValueIsAdjusting() || row < 0) return;

		int m_idx = tbl_session.convertRowIndexToModel(row);

		Object o_rep = tbl_session.getModel().getValueAt(m_idx, 0);
		if (o_rep instanceof RepetoireEntry) {
			JTabbedPane tp_parent = get_parent_tabbed_pane();
			if (tp_parent == null) return;

			RepetoireEntry rep = (RepetoireEntry) o_rep;

			tp_parent.add(new ReportRepetoire(rep));
//			do {
//				cntr = cntr.getParent();
//			} while (cntr.
//			new ReportRepetoire(rep);
		}
	}

	private JTabbedPane get_parent_tabbed_pane()
	{
		Container cntr = tbl_session;
		JTabbedPane tp = null;

		do {
			String cname = cntr.getName();
			if (cname != null && cname.equals("PtTabbedPane")) {
				tp = (JTabbedPane) cntr;
				break;
			}
		} while ((cntr = cntr.getParent()) != null);

		return tp;
	}
}
