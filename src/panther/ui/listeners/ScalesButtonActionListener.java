package panther.ui.listeners;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import panther.db.*;
import panther.ui.reports.*;

public class ScalesButtonActionListener
	implements ActionListener
{
	JTable tbl_students;

	public ScalesButtonActionListener(JTable tbl_students)
	{
		this.tbl_students = tbl_students;
	}

	public void actionPerformed(ActionEvent e)
	{
		String cmd = e.getActionCommand();

		if (cmd.equals("genreport.scales")) {
			int row = tbl_students.getSelectedRow();
			if (row < 0) return;

			int m_idx = tbl_students.convertRowIndexToModel(row);

			Object o_student = tbl_students.getModel().getValueAt(m_idx, 0);
			if (o_student == null || !(o_student instanceof Student))
				return;

			int id_student = ((Student) o_student).get_id();

			JTabbedPane tp_parent = get_parent_tabbed_pane();
			if (tp_parent == null)
				return;

			tp_parent.add(new ReportScales(id_student));
		}
	}

	private JTabbedPane get_parent_tabbed_pane()
	{
		Container cntr = tbl_students;
		JTabbedPane tp = null;

		do {
			String cname = cntr.getName();
			if (cname != null && cname.equals("PtTabbedPane")) {
				tp = (JTabbedPane) cntr;
				break;
			}
		} while ((cntr = cntr.getParent()) != null);

		return tp;
	}
}
