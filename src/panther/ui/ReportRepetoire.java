package panther.ui;

import java.awt.BorderLayout;
import javax.swing.*;

import org.jfree.chart.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.data.category.*;

import panther.db.*;

public class ReportRepetoire
	extends JPanel
{
	public ReportRepetoire(RepetoireEntry rep)
	{
		setName(rep.toString());
		setLayout(new BorderLayout());

		CategoryDataset dataset = gen_category_dataset(rep);
		JFreeChart chart = gen_chart(dataset);
		ChartPanel cpanel = new ChartPanel(chart);
		add(cpanel, BorderLayout.CENTER);
	}

	private CategoryDataset gen_category_dataset(RepetoireEntry rep)
	{
		String[] series = {
			"Student",
			"Teacher"
		};

		String[] categories = {
			"Musicality",
			"Technique",
			"Memorization"
		};

		int[] m_student = rep.get_metrics_student();
		int[] m_teacher = rep.get_metrics_teacher();

		DefaultCategoryDataset ds = new DefaultCategoryDataset();

		for (int i=0; i<categories.length; i++) {
			ds.addValue(m_student[i], series[0], categories[i]);
			ds.addValue(m_teacher[i], series[1], categories[i]);
		}

		return ds;
	}

	private JFreeChart gen_chart(CategoryDataset ds)
	{
		JFreeChart chart = ChartFactory.createBarChart(
				"Repetoire Bar Chart",
				"Metric",
				"Score",
				ds,
				PlotOrientation.VERTICAL,
				true, true, false
				);

		return chart;
	}
}
