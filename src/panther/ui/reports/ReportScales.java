package panther.ui.reports;

import java.util.*;
import java.sql.*;
import javax.swing.*;

import org.jfree.chart.*;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.xy.*;
import org.jfree.data.time.*;
import org.jfree.data.xy.*;

import panther.db.*;

public class ReportScales
	extends JPanel
{
	public ReportScales(int sid)
	{
		setName("Scales - " + MySQLManager.get_student_name(sid));
		ScalesList slist = MySQLManager.get_student_scales(sid);


		ChartPanel cp_proficiency = new ChartPanel(gen_chart(gen_dataset(
						slist,
						ScalesEntry.Type.STUDENT_PROFICIENCY,
						ScalesEntry.Type.TEACHER_PROFICIENCY),
					"Proficiency")
				);
		ChartPanel cp_tempo = new ChartPanel(gen_chart(gen_dataset(
						slist,
						ScalesEntry.Type.STUDENT_TEMPO,
						ScalesEntry.Type.TEACHER_TEMPO),
					"Tempo")
				);
		ChartPanel cp_fingering = new ChartPanel(gen_chart(gen_dataset(
						slist,
						ScalesEntry.Type.STUDENT_FINGERING,
						ScalesEntry.Type.TEACHER_FINGERING),
					"Fingering")
				);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(cp_proficiency);
		add(cp_tempo);
		add(cp_fingering);
	}

	private JFreeChart gen_chart(XYDataset dataset, String title)
	{
		JFreeChart chart = ChartFactory.createTimeSeriesChart(
				title,
				"Date",
				"Score",
				dataset,
				true, true, false
				);

		return chart;
	}

	private XYDataset gen_dataset(
			ScalesList slist,
			ScalesEntry.Type t_student,
			ScalesEntry.Type t_teacher)
	{
		ArrayList<java.sql.Date> slist_dates = slist.get_scales_list_dates();

		int[] slist_student = slist.get_scores(t_student);
		int[] slist_teacher = slist.get_scores(t_teacher);

		TimeSeries ts_student = new TimeSeries("Student");
		TimeSeries ts_teacher = new TimeSeries("Teacher");

		for (int i=0; i<slist_dates.size(); i++) {
			Week week = new Week(slist_dates.get(i));

			ts_student.add(week, slist_student[i]);
			ts_teacher.add(week, slist_teacher[i]);
		}

		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(ts_student);
		dataset.addSeries(ts_teacher);

		return dataset;
	}
}
