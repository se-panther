package panther.db;

import java.sql.*;

public class ScalesEntry
{
	public static enum Type
	{
		STUDENT_PROFICIENCY("Proficiency_S", "Student Proficiency", 0),
		TEACHER_PROFICIENCY("Proficiency_T", "Teacher Proficiency", 1),
		STUDENT_TEMPO("Tempo_S", "Student Tempo", 2),
		TEACHER_TEMPO("Tempo_T", "Teacher Tempo", 3),
		STUDENT_FINGERING("Fingering_S", "Student Fingering", 4),
		TEACHER_FINGERING("Fingering_T", "Teacher Fingering", 5);

		private final String sql_column;
		private final String label;
		private final int idx;

		Type(String sql_column, String label, int idx)
		{
			this.sql_column = sql_column;
			this.label = label;
			this.idx = idx;
		}

		public String sql_column() { return sql_column; }
		public String label() { return label; }
		public int idx() { return idx; }
	}

	private int id_scales;
	private int id_student;
	private Date date;
	private int[] metrics;

	public ScalesEntry(ResultSet rs)
		throws SQLException
	{
		int n_types = 0;
		for (Type t : Type.values()) {
			n_types++;
		}
		metrics = new int[n_types];

		id_scales	= rs.getInt("idScales");
		date		= rs.getDate("Date");

		for (Type t : Type.values()) {
			metrics[t.idx()] = rs.getInt(t.sql_column());
		}

		id_student = rs.getInt("Student_idStudent");
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Scales ").append(id_scales).append(": ").append(date).
			append("\n\tProficiency (S/T): \t").
			append(get_metric_score(Type.STUDENT_PROFICIENCY)).append(" / ").
			append(get_metric_score(Type.TEACHER_PROFICIENCY)).
			append("\n\tTempo (S/T): \t").
			append(get_metric_score(Type.STUDENT_TEMPO)).append(" / ").
			append(get_metric_score(Type.TEACHER_TEMPO)).
			append("\n\tFingering (S/T): \t").
			append(get_metric_score(Type.STUDENT_FINGERING)).append(" / ").
			append(get_metric_score(Type.TEACHER_FINGERING));

		return sb.toString();
	}

	public Date get_date()
	{
		return date;
	}

	public int get_metric_score(Type t)
	{
		return metrics[t.idx()];
	}
}
