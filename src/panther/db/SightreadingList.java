import java.util.ArrayList;
import java.sql.*;

public class SightreadingList
	extends ArrayList<SightreadingEntry>
{
	public SightreadingList(ResultSet rset)
		throws SQLException
	{
		while (rset.next()) {
			add(new SightreadingEntry(rset));
		}
	}
}
