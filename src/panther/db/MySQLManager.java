package panther.db;

import java.util.ArrayList;
import java.sql.*;

public class MySQLManager
{
	private static final String	DB_SCHEMA	= "team3";
	private static final String DB_URL	= "jdbc:mysql://robocomp.dyndns.org/" + DB_SCHEMA;
	private static final String DB_USER	= "team3";
	private static final String DB_PASS	= "team3";

	private static Connection conn;

	static {
		conn = null;

		try {
			conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
			System.out.println("Database connected");
		}
		catch (SQLException se) {
			se.printStackTrace();
		}
	}

	public MySQLManager()
	{
	}


	public static ArrayList<Student> get_students()
	{
		ArrayList<Student> students = null;

		try {
			students = new ArrayList<Student>();

			Statement st = conn.createStatement();
			st.executeQuery("select * from student order by LName;");

			ResultSet rs = st.getResultSet();

			while (rs.next()) {
				students.add(new Student(rs));
			}
		}
		catch (SQLException se) {
			students = null;
			se.printStackTrace();
		}

		return students;
	}

	public static RepetoireList get_student_repetoire(int sid)
	{
		RepetoireList rlist = null;

		try {
			Statement st = conn.createStatement();
			st.executeQuery("select * from repetoire where Student_idStudent=" + 
					sid + " order by Date;");

			ResultSet rs = st.getResultSet();

			rlist = new RepetoireList(rs);
		}
		catch (SQLException se) {
			se.printStackTrace();
		}

		return rlist;
	}

	private static ResultSet get_result_set(String query)
	{
		ResultSet rs = null;

		try {
			Statement st = conn.createStatement();
			st.executeQuery(query);

			rs = st.getResultSet();
		}
		catch (SQLException se) {
			se.printStackTrace();
		}

		return rs;
	}

	public static ScalesList get_student_scales(int sid)
	{
		ScalesList slist = null;
		ResultSet rs = get_result_set(
				"select * from scales where Student_idStudent=" +
				sid +
				" order by Date;"
				);

		try {
			slist = new ScalesList(rs);
		}
		catch (SQLException se) {
			se.printStackTrace();
		}

		return slist;
	}
	
	public static String get_student_name(int sid)
	{
		String name = "";
		try {
			ResultSet rs = get_result_set(
					"select FName, LName from student where idStudent=" +
					sid + ";");

			if (rs == null || !rs.next())
				return name;

			name = rs.getString("LName") + ", " + rs.getString("FName");
		}
		catch (SQLException se) {
			se.printStackTrace();
		}

		return name;
	}
}
