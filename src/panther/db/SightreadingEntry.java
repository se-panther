import java.sql.*;

public class SightreadingEntry
{
	private final static int N_METRICS	= 8;

	private final static int IDX_EYESONMUSIC_S	= 0;
	private final static int IDX_EYESONMUSIC_T	= 1;
	private final static int IDX_LOOKINGAHEAD_S	= 2;
	private final static int IDX_LOOKINGAHEAD_T	= 3;
	private final static int IDX_ANALYSIS_S		= 4;
	private final static int IDX_ANALYSIS_T		= 5;
	private final static int IDX_TEMPO_S		= 6;
	private final static int IDX_TEMPO_T		= 7;
	private final static int IDX_PULSE_S		= 8;
	private final static int IDX_PULSE_T		= 9;
	private final static int IDX_TONE_S			= 10;
	private final static int IDX_TONE_T			= 11;
	private final static int IDX_FINGERING_S	= 12;
	private final static int IDX_FINGERING_T	= 13;
	private final static int IDX_OTHER_S		= 14;
	private final static int IDX_OTHER_T		= 15;

	private Date date;
	private int id_sightreading;
	private int id_student;
	private int[] metrics;

	public SightreadingEntry(ResultSet rs)
		throws SQLException
	{
		metrics = new int[N_METRICS * 2];

		date = rs.getDate("Date");

		metrics[IDX_EYESONMUSIC_S]	= rs.getInt("EyesOnMusic_S");
		metrics[IDX_EYESONMUSIC_T]	= rs.getInt("EyesOnMusic_T");
		metrics[IDX_LOOKINGAHEAD_S]	= rs.getInt("LookingAhead_S");
		metrics[IDX_LOOKINGAHEAD_T]	= rs.getInt("LookingAhead_T");
		metrics[IDX_ANALYSIS_S]		= rs.getInt("Analysis_S");
		metrics[IDX_ANALYSIS_T]		= rs.getInt("Analysis_T");
		metrics[IDX_TEMPO_S]		= rs.getInt("Tempo_S");
		metrics[IDX_TEMPO_T]		= rs.getInt("Tempo_T");
		metrics[IDX_PULSE_S]		= rs.getInt("Pulse_S");
		metrics[IDX_PULSE_T]		= rs.getInt("Pulse_T");
		metrics[IDX_TONE_S]			= rs.getInt("Tone_S");
		metrics[IDX_TONE_T]			= rs.getInt("Tone_T");
		metrics[IDX_FINGERING_S]	= rs.getInt("Fingering_S");
		metrics[IDX_FINGERING_T]	= rs.getInt("Fingering_T");
		metrics[IDX_OTHER_S]		= rs.getInt("Other_S");
		metrics[IDX_OTHER_T]		= rs.getInt("Other_T");

		id_sightreading = rs.getInt("idSightReading");
		id_student = rs.getInt("Student_idStudent");
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Sightreading ").append(id_sightreading).append(": ").
			append(date).
			append("\n\tEyes on music (S/T):\t").
			append(metrics[IDX_EYESONMUSIC_S]).append(" / ").
			append(metrics[IDX_EYESONMUSIC_T]).
			append("\n\tLooking ahead (S/T):\t").
			append(metrics[IDX_LOOKINGAHEAD_S]).append(" / ").
			append(metrics[IDX_LOOKINGAHEAD_T]).
			append("\n\tAnalysis (S/T):\t").
			append(metrics[IDX_ANALYSIS_S]).append(" / ").
			append(metrics[IDX_ANALYSIS_T]).
			append("\n\tTempo (S/T):\t").
			append(metrics[IDX_TEMPO_S]).append(" / ").
			append(metrics[IDX_TEMPO_T]).
			append("\n\tPulse (S/T):\t").
			append(metrics[IDX_PULSE_S]).append(" / ").
			append(metrics[IDX_PULSE_T]).
			append("\n\tTone (S/T):\t").
			append(metrics[IDX_TONE_S]).append(" / ").
			append(metrics[IDX_TONE_T]).
			append("\n\tFingering (S/T):\t").
			append(metrics[IDX_FINGERING_S]).append(" / ").
			append(metrics[IDX_FINGERING_T]).
			append("\n\tOther (S/T):\t").
			append(metrics[IDX_OTHER_S]).append(" / ").
			append(metrics[IDX_OTHER_T]);

		return sb.toString();
	}
}
