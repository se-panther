package panther.db;

import java.util.ArrayList;
import java.sql.*;

public class RepetoireList
	extends ArrayList<RepetoireEntry>
{
	public RepetoireList(ResultSet rset)
		throws SQLException
	{
		while (rset.next()) {
			add(new RepetoireEntry(rset));
		}
	}
}
