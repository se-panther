package panther.db;

import java.util.*;
import java.sql.*;

public class ScalesList
	extends ArrayList<ScalesEntry>
{
	public ScalesList(ResultSet rset)
		throws SQLException
	{
		while (rset.next()) {
			add(new ScalesEntry(rset));
		}
	}

	public int[] get_scores(ScalesEntry.Type type)
	{
		int[] scores = new int[size()];

		for (int i=0; i<scores.length; i++) {
			scores[i] = get(i).get_metric_score(type);
		}

		return scores;
	}

	public ArrayList<java.sql.Date> get_scales_list_dates()
	{
		ArrayList<java.sql.Date> list = new ArrayList<java.sql.Date>();

		Iterator<ScalesEntry> iter = iterator();
		while (iter.hasNext()) {
			ScalesEntry entry = iter.next();

			list.add(entry.get_date());
		}
		
		return list;
	}
}
