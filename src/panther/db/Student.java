package panther.db;

import java.util.*;
import java.sql.*;

public class Student
{
	private int id;
	private String fname;
	private String lname;
	private int year;
	private String email;

	public Student(ResultSet rs)
		throws SQLException
	{
		id = rs.getInt("idStudent");
		fname = rs.getString("FName");
		lname = rs.getString("LName");
		year = rs.getInt("Year");
		email  =rs.getString("email");
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();

		sb.append(id).append(" ").append(lname).append(", ").append(fname);

		return sb.toString();
	}

//	public String getName()
//	{ return name; }

	public int get_id()
	{ return id; }
}
