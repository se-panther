package panther.db;

import java.sql.*;

public class RepetoireEntry
{
	public static enum Type
	{
		STUDENT_MUSICALITY("Musicality_S", "Student Musicality", 0),
		TEACHER_MUSICALITY("Musicality_T", "Teacher Musicality", 1),
		STUDENT_TECHNIQUE("Technique_S", "Student Technique", 2),
		TEACHER_TECHNIQUE("Technique_T", "Teacher Technique", 3),
		STUDENT_MEMORIZATION("Memorization_S", "Student Memorization", 4),
		TEACHER_MEMORIZATION("Memorization_T", "Teacher Memorization", 5);

		private final String sql_column;
		private final String label;
		private final int idx;

		Type(String sql_column, String label, int idx)
		{
			this.sql_column = sql_column;
			this.label = label;
			this.idx = idx;
		}

		public String sql_column() { return sql_column; }
		public String label() { return label; }
		public int idx() { return idx; }
	}

	private final static int N_METRICS			= 3;

	private final static int IDX_MUSICALITY		= 0;
	private final static int IDX_TECHNIQUE		= 1;
	private final static int IDX_MEMORIZATION	= 2;

	private String	piece_name;
	private Date	date;
	private int		id_repetoire;
	private int		id_student;
	private int[]	metrics_student;
	private int[]	metrics_teacher;

	public RepetoireEntry(ResultSet rset)
		throws SQLException
	{
		metrics_student = new int[N_METRICS];
		metrics_teacher = new int[N_METRICS];

		piece_name	= rset.getString("PieceName");
		date		= rset.getDate("Date");

		metrics_student[IDX_MUSICALITY] 	= rset.getInt("Musicality_S");
		metrics_student[IDX_TECHNIQUE]		= rset.getInt("Technique_S");
		metrics_student[IDX_MEMORIZATION]	= rset.getInt("Memorization_S");

		metrics_teacher[IDX_MUSICALITY]		= rset.getInt("Musicality_T");
		metrics_teacher[IDX_TECHNIQUE]		= rset.getInt("Technique_T");
		metrics_teacher[IDX_MEMORIZATION]	= rset.getInt("Memorization_T");

		id_repetoire = rset.getInt("idRepetoire");
		id_student = rset.getInt("Student_idStudent");
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Repetoire ").append(date).append(": " + piece_name);

		return sb.toString();
//		return piece_name + ": " + date + "\n" +
//				"\tMusicality (S/T): \t" + metrics_student[IDX_MUSICALITY] +
//				" / " + metrics_teacher[IDX_MUSICALITY] + "\n" +
//				"\tTechnique (S/T): \t" + metrics_student[IDX_TECHNIQUE] +
//				" / " + metrics_teacher[IDX_TECHNIQUE] + "\n" +
//				"\tMemorization (S/T): \t" + metrics_student[IDX_MEMORIZATION] +
//				" / " + metrics_teacher[IDX_MEMORIZATION];
	}

	public int[] get_metrics_student()
	{ return metrics_student; }

	public int[] get_metrics_teacher()
	{ return metrics_teacher; }
}
