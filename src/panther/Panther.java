package panther;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.jfree.ui.ApplicationFrame;

import panther.ui.*;
import panther.event.*;

public class Panther
	extends ApplicationFrame
{
	public static void main(String[] args)
	{
		/* Setup our GUI within the event dispatching thread.
		 * see Swing's Threading Policy */
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				Panther panther = new Panther();

				panther.pack();
				panther.setVisible(true);
			}
		});
	}

	public Panther()
	{
		super("Panther");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// add our menu bar... Just provides 'quit' functionality for now
		setJMenuBar(new PtMenuBar(new PtActionListener(this)));

		// every thing else is displayed within our tabbed pane
		add(new PtTabbedPane(), BorderLayout.CENTER);
	}
}
