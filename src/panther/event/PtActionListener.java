package panther.event;

import java.awt.Window;
import java.awt.event.*;

public class PtActionListener
	implements ActionListener
{
	private Window pwindow;

	public PtActionListener(Window pwindow)
	{
		this.pwindow = pwindow;
	}

	public void actionPerformed(ActionEvent e)
	{
		String action = e.getActionCommand();
		System.out.println(action);

		if (action.equals("quit")) {
			pwindow.dispose();
		}
	}
}
