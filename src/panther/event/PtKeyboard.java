package panther.event;

import java.awt.Window;
import java.awt.event.*;

public class PtKeyboard
	extends KeyAdapter
{
	private Window pwindow;

	private boolean mod_ctrl;

	public PtKeyboard(Window pwindow)
	{
		this.pwindow = pwindow;

		mod_ctrl = false;
	}

	public void keyPressed(KeyEvent e)
	{
		System.out.println(e.getKeyChar());

		switch (e.getKeyCode()) {
			case KeyEvent.VK_Q:
				if (e.isControlDown()) {
					pwindow.dispose();
				}
				break;
		}
	}
}
